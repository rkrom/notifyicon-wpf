﻿// hardcodet.net NotifyIcon for WPF
// Copyright (c) 2009 - 2013 Philipp Sumi
// Contact and Information: http://www.hardcodet.net
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the Code Project Open License (CPOL);
// either version 1.0 of the License, or (at your option) any later
// version.
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Image = System.Windows.Controls.Image;
using Point = System.Windows.Point;
using Size = System.Windows.Size;

namespace Hardcodet.Wpf.TaskbarNotification
{
    /// <summary>
    /// Extension to support using different types to set the tray Icon
    /// With added FrameworkElement support, one now can use the PackIcon from
    /// <a href="https://github.com/ControlzEx/ControlzEx">ControlzEx</a>
    /// and
    /// <a href="https://github.com/MahApps/MahApps.Metro.IconPacks">MahApps.Metro.IconPacks</a>
    /// </summary>
    public static class IconExtensions
    {
        /// <summary>
        /// Reads a given image resource into a WinForms icon.
        /// </summary>
        /// <param name="imageSource">Image source pointing to an icon file (*.ico).</param>
        /// <returns>An icon object that can be used with the taskbar area.</returns>
        public static Icon ToIcon(this ImageSource imageSource)
        {
            if (imageSource == null) return null;

            Uri resourceUri;

            if (!Uri.TryCreate(imageSource.ToString(), UriKind.RelativeOrAbsolute, out resourceUri))
            {
                // Check if the supplied ImageSource is a BitmapImage
                var bimapImage = imageSource as BitmapImage;
                if (bimapImage != null)
                {
                    // Get the UriSource
                    resourceUri = bimapImage.UriSource;
                }
            }
            // Check if there is an Uri which can be used to create an icon from
            if (resourceUri != null)
            {
                var streamInfo = Application.GetResourceStream(resourceUri);
                if (streamInfo != null)
                {
                    // Try to create an icon from the stream we gotten via the Uri
                    try
                    {
                        return new Icon(streamInfo.Stream);
                    }
                    catch
                    {
                        // Ignoring for now, we can make an Icon differently
                    }
                }
            }


            // Create an icon from the representation of the imageSource by creating an Image (wpf) and render it to a RenderTargetBitmap

            var image = new Image
            {
                Source = imageSource
            };
            return image.ToIcon();
        }

        /// <summary>
        ///     Render a frameworkElement to a "GDI" Icon with a specified size or the system's default size.
        /// </summary>
        /// <param name="frameworkElement">FrameworkElement</param>
        /// <param name="size">Optional, specifies the size, if not given the system default is used</param>
        /// <returns>Icon</returns>
        public static Icon ToIcon(this FrameworkElement frameworkElement, int? size = null)
        {
            if (frameworkElement == null)
            {
                throw new ArgumentNullException(nameof(frameworkElement));
            }

            var memoryStream = frameworkElement.ToIconMemoryStream(size.HasValue ? new[] { size.Value } : null);
            return new Icon(memoryStream);
        }

        /// <summary>
        /// Create a "GDI" icon from the supplied FrameworkElement, it is possible to specify multiple icon sizes.
        /// Note: this doesn't work on Windows versions BEFORE Windows Vista!
        /// </summary>
        /// <param name="frameworkElement">FrameworkElement to convert to an icon</param>
        /// <param name="optionalIconSizes">Optional, IEnumerable with icon sizes, default Icon sizes (as specified by windows): 16x16, 32x32, 48x48, 256x256</param>
        /// <returns>MemoryStream with the icon data</returns>
        public static MemoryStream ToIconMemoryStream(this FrameworkElement frameworkElement, IEnumerable<int> optionalIconSizes = null)
        {
            if (frameworkElement == null)
            {
                throw new ArgumentNullException(nameof(frameworkElement));
            }
            // Use the supplied or default values for the icon sizes
            var iconSizes = optionalIconSizes != null ? new List<int>(optionalIconSizes) : new List<int> {16, 32, 48, 256};

            var bitmapFrames = new List<BitmapFrame>();
            foreach (var iconSize in iconSizes)
            {
                var currentSize = new Size(iconSize, iconSize);
                var bitmapSource = frameworkElement.ToBitmapSource(currentSize);
                bitmapFrames.Add(BitmapFrame.Create(bitmapSource));
            }
            return bitmapFrames.ToIconMemoryStream();
        }

        /// <summary>
        /// Helper method to write one more BitmapFrames to a MemoryStream with GDI Icon data
        /// This can be written to a .ico or used with new Icon(stream)
        /// </summary>
        /// <param name="bitmapFrames">IList of BitmapFrames</param>
        /// <returns>MemoryStream with the icon data</returns>
        public static MemoryStream ToIconMemoryStream(this IList<BitmapFrame> bitmapFrames)
        {
            var stream = new MemoryStream();
            var binaryWriter = new BinaryWriter(stream);
            //
            // ICONDIR structure
            //
            binaryWriter.Write((short)0); // reserved
            binaryWriter.Write((short)1); // image type (icon)
            binaryWriter.Write((short)bitmapFrames.Count); // number of images

            //
            // ICONDIRENTRY structure
            //
            const int iconDirSize = 6;
            const int iconDirEntrySize = 16;

            var imageSizes = new List<Size>();
            var encodedImages = new List<MemoryStream>();
            foreach (var bitmapFrame in bitmapFrames)
            {

                imageSizes.Add(new Size(bitmapFrame.Width, bitmapFrame.Height));
                var imageStream = new MemoryStream();
                // Use PngBitmapEncoder for icons, with this we also respect transparency.
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(bitmapFrame);
                encoder.Save(imageStream);
                // Make sure the stream is read from the beginning
                imageStream.Seek(0, SeekOrigin.Begin);
                // Store the stream for later
                encodedImages.Add(imageStream);
            }

            var offset = iconDirSize + (imageSizes.Count*iconDirEntrySize);
            for (var i = 0; i < imageSizes.Count; i++)
            {
                var imageSize = imageSizes[i];
                // Write the width / height, 0 means 256
                binaryWriter.Write((int) imageSize.Width == 256 ? (byte) 0 : (byte) imageSize.Width);
                binaryWriter.Write((int) imageSize.Height == 256 ? (byte) 0 : (byte) imageSize.Height);
                binaryWriter.Write((byte) 0); // no pallete
                binaryWriter.Write((byte) 0); // reserved
                binaryWriter.Write((short) 0); // no color planes
                binaryWriter.Write((short) 32); // 32 bpp
                binaryWriter.Write((int)encodedImages[i].Length); // image data length
                binaryWriter.Write(offset);
                offset += (int)encodedImages[i].Length;
            }

            binaryWriter.Flush();
            //
            // Write image data
            //
            foreach (var encodedImage in encodedImages)
            {
                encodedImage.WriteTo(stream);
                encodedImage.Dispose();
            }
            // Rewind to make the MemoryStream usable
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        /// <summary>
        ///     Render the frameworkElement to a BitmapSource
        /// </summary>
        /// <param name="frameworkElement">FrameworkElement</param>
        /// <param name="size">Size, using the bound as size by default</param>
        /// <param name="dpiX">Horizontal DPI settings</param>
        /// <param name="dpiY">Vertical DPI settings</param>
        /// <returns>BitmapSource</returns>
        public static BitmapSource ToBitmapSource(this FrameworkElement frameworkElement, Size? size = null, double dpiX = 96.0, double dpiY = 96.0)
        {
            if (frameworkElement == null)
            {
                throw new ArgumentNullException(nameof(frameworkElement));
            }
            // Make sure we have a size
            if (!size.HasValue)
            {
                var bounds = VisualTreeHelper.GetDescendantBounds(frameworkElement);
                size = bounds != Rect.Empty ? bounds.Size : new Size(16, 16);
            }

            // Create a viewbox to render the frameworkElement in the correct size
            var viewbox = new Viewbox
            {
                //frameworkElement to render
                Child = frameworkElement
            };
            viewbox.Measure(size.Value);
            viewbox.Arrange(new Rect(new Point(), size.Value));
            viewbox.UpdateLayout();

            var renderTargetBitmap = new RenderTargetBitmap((int) (size.Value.Width*dpiX/96.0),
                (int) (size.Value.Height*dpiY/96.0),
                dpiX,
                dpiY,
                PixelFormats.Pbgra32);
            var drawingVisual = new DrawingVisual();
            using (var drawingContext = drawingVisual.RenderOpen())
            {
                var visualBrush = new VisualBrush(viewbox);
                drawingContext.DrawRectangle(visualBrush, null, new Rect(new Point(), size.Value));
            }
            renderTargetBitmap.Render(drawingVisual);
            // Disassociate the frameworkElement from the viewbox, so the frameworkElement could be used elsewhere
            viewbox.RemoveChild(frameworkElement);
            return renderTargetBitmap;
        }

        /// <summary>
        /// Disassociate the child from the parent
        /// </summary>
        /// <param name="parent">DependencyObject</param>
        /// <param name="child">UIElement</param>
        private static void RemoveChild(this DependencyObject parent, UIElement child)
        {
            var panel = parent as Panel;
            if (panel != null)
            {
                panel.Children.Remove(child);
                return;
            }

            var decorator = parent as Decorator;
            if (decorator != null)
            {
                if (ReferenceEquals(decorator.Child, child))
                {
                    decorator.Child = null;
                }
                return;
            }

            var contentPresenter = parent as ContentPresenter;
            if (contentPresenter != null)
            {
                if (Equals(contentPresenter.Content, child))
                {
                    contentPresenter.Content = null;
                }
                return;
            }

            var contentControl = parent as ContentControl;
            if (contentControl != null)
            {
                if (Equals(contentControl.Content, child))
                {
                    contentControl.Content = null;
                }
            }
        }
    }
}